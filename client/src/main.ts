import { createApp } from "vue";
import { createPinia } from "pinia";

import "./style.css";

import PrimeVue from "primevue/config";
import Button from "primevue/button";
import "primevue/resources/themes/arya-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";

import App from "./App.vue";

const app = createApp(App);
const pinia = createPinia();

app.use(PrimeVue, { unstayled: true });
app.component("Button", Button);

app.use(pinia);

app.mount("#app");
